﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token : MonoBehaviour
{
    public int points = 1; //amt of points token gives
    public float speed;

    private void Update()
    {
        //will move at set speed
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //to see if the collision was the player
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<Player>().score += points; //add to score
            Debug.Log(collision.GetComponent<Player>().score); //see if its working in console
            Destroy(gameObject);
        }
    }
}
