﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] obstaclePatterns; //array for different pattern objects
    private float timeBetweenSpawn;
    public float startTimeBetweenSpawn;
    public float decreaseTime;
    public float minTime = .7f; //so obstacles cant come every second

    private void Update()
    {
        if(timeBetweenSpawn <= 0)
        {
            int rand = Random.Range(0, obstaclePatterns.Length); //determines random time obs will spawn
            Instantiate(obstaclePatterns[rand], transform.position, Quaternion.identity); //instantiates random pattern
            timeBetweenSpawn = startTimeBetweenSpawn;
            if(startTimeBetweenSpawn > minTime) //if one didnt just come
            {
                startTimeBetweenSpawn -= decreaseTime;
            }
        }
        else
        {
            timeBetweenSpawn -= Time.deltaTime; //constantly update time
        }
    }
}
