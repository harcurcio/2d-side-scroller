﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int speed = 10;
    public int health = 4;
    public int score = 0;

    private float moveOnY; //specifically the y axis
    private Rigidbody2D rb;
    public Text healthUI;
    public Text scoreUI;

    public GameObject effect; //collision effect\
    public AudioSource sound;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); //establishing player as rb2d
        sound = GetComponent<AudioSource>(); //est sound as an audisource
    }

    // Update is called once per frame
    void Update()
    {
        healthUI.text = "Health: " + health.ToString(); //tostring makes the int printable
        scoreUI.text = "Score: " + score.ToString();
        if(health <= 0) //ya dead
        {
            SceneManager.LoadScene("GameOver"); //shows gameover scene
        }
        if(score==10) //winning condition
        {
            SceneManager.LoadScene("YouWin");
        }
        moveOnY = Input.GetAxis("Vertical"); //a class I found that worked for vertical movement
        rb.velocity = new Vector2(0, moveOnY * speed); //so the movement is fluid
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //to see if the collision was the player
        if (other.gameObject.tag=="Obstacle")
        {
            health--;
            sound.Play();
            Instantiate(effect, transform.position, Quaternion.identity); //creates the collision effect
            //quaternion.identity = no rotation for the object
            Destroy(other.gameObject);
        }
    }
}
