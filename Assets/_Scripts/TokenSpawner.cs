﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenSpawner : MonoBehaviour
{
    public GameObject token;

        public float maxTime = 5;
        public float minTime = 2;

        private float time;
        private float spawnTime;

        void Start()
        {
            SetRandomTime();
            time = minTime;
        }

        void FixedUpdate()
        {

            time += Time.deltaTime; //counts up time

            if (time >= spawnTime)  //check if its the right time to spawn the object
        {
            SpawnObject();
                SetRandomTime();
            }

        }

        //spawns the object and resets the time
        void SpawnObject()
        {
            time = 0; 
            Instantiate(token, transform.position, token.transform.rotation);
        }

        //sets random time between minTime and maxTime
        void SetRandomTime()
        {
            spawnTime = Random.Range(minTime, maxTime);
        }
}
