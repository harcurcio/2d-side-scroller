﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    //damage the obstacle gives
    public int damage = 1;
    public float speed;

    public GameObject effect; //makes the collision effect

    private void Update()
    {
        //will move at set speed
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    void OnCollisionEnter2D(Collider2D other)
    {
        //to see if the collision was the player
        if(other.gameObject.tag == "Player")
        {
            Instantiate(effect, transform.position, Quaternion.identity); //creates the collision effect
        }
    }
}
