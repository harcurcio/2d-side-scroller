A 2D side-scrolling game developed using Unity Engine and C#. Play as a ghost who has lost their way and needs to get back home. Collect coins to win and avoid the evil bats. Good luck!
